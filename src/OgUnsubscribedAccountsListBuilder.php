<?php

namespace Drupal\og_message;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Build a listing of Members that unsubscribed to the notification entities.
 *
 * @ingroup og_message
 */
class OgUnsubscribedAccountsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Members that unsubscirbed to the groups notification ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\og_message\Entity\OgUnsubscribedAccounts $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.og_unsubscribed_accounts.edit_form',
      ['og_unsubscribed_accounts' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
