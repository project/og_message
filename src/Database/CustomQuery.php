<?php

namespace Drupal\og_message\Database;

/**
 * Defines CustomQuery class.
 */
class CustomQuery {

  /**
   * Standard Drupal database connection, for making queries against.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->connection = \Drupal::database();
  }

  /**
   * Fetch user account that unsubscribed to group notification.
   *
   * @param int $group_id
   *   The ID of the group to check against.
   *
   * @return array
   *   List of user IDs who unsubscribe to group notifications.
   */
  public function getUnsubscribeAccounts($group_id) {
    $result = $this->connection->select('og_unsubscribed_accounts', 'u')
      ->condition('u.group', $group_id, '=')
      ->fields('u', ['user']);

    return $result->execute()->fetchCol();
  }

  /**
   * All group members are initially subscribed to a notificaiton message.
   *
   * @param int $group_id
   *   The ID of the group to check against.
   *
   * @return array
   *   All users IDs who are a member of this group.
   */
  public function getSubscribeAccounts($group_id) {
    $result = $this->connection->select('og_membership', 'u')
      ->condition('u.entity_id', $group_id, '=')
      ->fields('u', ['uid']);

    return $result->execute()->fetchCol();
  }

  /**
   * List all group a user is member of.
   *
   * @return array
   *   All groups this user is a member of.
   */
  public function getUserGroups($uid) {
    $result = \Drupal::entityTypeManager()
      ->getStorage('og_membership')
      ->loadByProperties(['uid' => $uid]);

    return $result;
  }

  /**
   * Get subscribed members.
   *
   * @param int $group_id
   *   The ID of the group the user will be added to.
   *
   * @return array
   *   List of users objects who subscribe to group notifications.
   */
  public function getSubscribers($group_id) {
    $groupMembers = $this->getSubscribeAccounts($group_id);
    $unsubscribedAccounts = $this->getUnsubscribeAccounts($group_id);
    $uids = [];
    foreach ($groupMembers as $member) {
      if (in_array($member, $unsubscribedAccounts)) {
        continue;
      }
      $uids[] = $member;
    }
    return $this->getUsers($uids);
  }

  /**
   * Fetch users by user ID.
   *
   * @param int $uids
   *   The user IDs to load.
   *
   * @return array
   *   Array of user accounts.
   */
  public function getUsers($uids) {
    $accounts = [];
    if (!empty($uids)) {
      $accounts = \Drupal::entityTypeManager()
        ->getStorage('user')
        ->loadByProperties(['uid' => $uids]);
    }

    return $accounts;
  }

  /**
   * Checks is user is subscribe.
   *
   * @param int $group_id
   *   The ID of the group to check.
   * @param int $uid
   *   The user ID to check.
   *
   * @return bool
   *   Returns TRUE if user is subscribe; else FALSE.
   */
  public function checkIfUserIsSubscribe($group_id, $uid) {
    $result = \Drupal::entityTypeManager()
      ->getStorage('og_unsubscribed_accounts')
      ->loadByProperties(['group' => $group_id, 'user' => $uid]);

    return (!$result) ? TRUE : FALSE;
  }

  /**
   * Saves an account to og_unsubscribed_accounts.
   *
   * @param int $group_id
   *   The ID of the group the user will be added to.
   * @param int $uid
   *   The user ID to add.
   *
   * @return object
   *   This query object.
   */
  public function unSubscribeUserAccount($group_id, $uid) {
    $unsubscribe = \Drupal::entityTypeManager()
      ->getStorage('og_unsubscribed_accounts')
      ->create(['group' => $group_id, 'user' => $uid])
      ->save();

    return $this;
  }

  /**
   * Removes an account to og_unsubscribed_accounts.
   *
   * @param int $group_id
   *   The ID of the group the user will be removed from.
   * @param int $uid
   *   The user ID to remove.
   *
   * @return CustomQuery
   *   This query object.
   */
  public function subscribeUserAccount($group_id, $uid) {
    $result = \Drupal::entityTypeManager()
      ->getStorage('og_unsubscribed_accounts')
      ->loadByProperties(['group' => $group_id, 'user' => $uid]);

    $unsubscribe = \Drupal::entityTypeManager()
      ->getStorage('og_unsubscribed_accounts')
      ->delete($result);

    return $this;
  }

}
