<?php

namespace Drupal\og_message;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Group message entity.
 *
 * @see \Drupal\og_message\Entity\GroupMessage.
 */
class GroupMessageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\og_message\Entity\GroupMessageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished group message entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published group message entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit group message entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete group message entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add group message entities');
  }

}
