<?php

namespace Drupal\og_message\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Members that unsubscribed.
 *
 * @ingroup og_message
 */
interface OgUnsubscribedAccountsInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the record creation timestamp.
   *
   * @return int
   *   Creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the record creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return \Drupal\og_message\Entity\OgUnsubscribedAccountsInterface
   *   The called entity.
   */
  public function setCreatedTime($timestamp);

}
