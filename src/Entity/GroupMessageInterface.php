<?php

namespace Drupal\og_message\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Group message entities.
 *
 * @ingroup og_message
 */
interface GroupMessageInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the Group message name.
   *
   * @return string
   *   Name of the Group message.
   */
  public function getName();

  /**
   * Sets the Group message name.
   *
   * @param string $name
   *   The Group message name.
   *
   * @return \Drupal\og_message\Entity\GroupMessageInterface
   *   The called Group message entity.
   */
  public function setName($name);

  /**
   * Gets the Group message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Group message.
   */
  public function getCreatedTime();

  /**
   * Sets the Group message creation timestamp.
   *
   * @param int $timestamp
   *   The Group message creation timestamp.
   *
   * @return \Drupal\og_message\Entity\GroupMessageInterface
   *   The called Group message entity.
   */
  public function setCreatedTime($timestamp);

}
