<?php

namespace Drupal\og_message\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Members that unsubscribed.
 */
class OgUnsubscribedAccountsViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
