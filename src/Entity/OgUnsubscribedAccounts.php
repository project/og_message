<?php

namespace Drupal\og_message\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Members that unsubscirbed to the groups notification entity.
 *
 * @ingroup og_message
 *
 * @ContentEntityType(
 *   id = "og_unsubscribed_accounts",
 *   label = @Translation("Members that unsubscribed to the groups notification"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\og_message\OgUnsubscribedAccountsListBuilder",
 *     "views_data" = "Drupal\og_message\Entity\OgUnsubscribedAccountsViewsData",
 *
 *     "access" = "Drupal\og_message\OgUnsubscribedAccountsAccessControlHandler",
 *   },
 *   base_table = "og_unsubscribed_accounts",
 *   translatable = FALSE,
 *   admin_permission = "administer og_unsubscribed_accounts",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 * )
 */
class OgUnsubscribedAccounts extends ContentEntityBase implements OgUnsubscribedAccountsInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['group'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Group'))
      ->setDescription(t('Group that you want to configure'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', "node")
      ->setSetting('handler_settings', ['target_bundles' => ['group' => 'group']])
      ->setTranslatable(TRUE);

    $fields['user'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('User that you want to configure'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
