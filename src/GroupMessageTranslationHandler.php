<?php

namespace Drupal\og_message;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for group_message.
 */
class GroupMessageTranslationHandler extends ContentTranslationHandler {
  // Override here the needed methods from ContentTranslationHandler.
}
