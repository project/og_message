<?php

namespace Drupal\og_message\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\og_message\Database\CustomQuery;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines GroupMessageController class.
 */
class GroupMessageController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function index() {
    $query = new CustomQuery();
    $groupsResult = $query->getUserGroups(\Drupal::currentUser()->id());
    $groups = [];

    foreach ($groupsResult as $group) {
      $isUserSubscribed = $query->checkIfUserIsSubscribe($group->getGroup()->id(), \Drupal::currentUser()->id());
      $groups[] = [
        'title' => $group->getGroup()->title->value,
        'id' => $group->getGroup()->id(),
        'is_subscribe' => $isUserSubscribed,
      ];
    }

    return [
      '#theme' => 'subscription_list',
      '#groups' => $groups,
      '#title' => 'Group subscription',
    ];
  }

  /**
   * Subscribe user to group notifications.
   *
   * @return Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect HTTP_REFERER.
   */
  public function subscribe($group) {
    $query = new CustomQuery();
    $query->subscribeUserAccount($group, \Drupal::currentUser()->id());
    return new RedirectResponse(\Drupal::request()->server->get('HTTP_REFERER'));
  }

  /**
   * Unsubscribe user to group notifications.
   *
   * @return Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect HTTP_REFERER.
   */
  public function unsubscribe(int $group) {
    $query = new CustomQuery();
    $isUserSubscribed = $query->checkIfUserIsSubscribe($group, \Drupal::currentUser()->id());
    if ($isUserSubscribed) {
      $query->unSubscribeUserAccount($group, \Drupal::currentUser()->id());
    }
    return new RedirectResponse(\Drupal::request()->server->get('HTTP_REFERER'));
  }

}
