<?php

namespace Drupal\og_message;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Members that unsubscribed to the notifications.
 *
 * @see \Drupal\og_message\Entity\OgUnsubscribedAccounts.
 */
class OgUnsubscribedAccountsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\og_message\Entity\OgUnsubscribedAccountsInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished members that unsubscirbed to the groups notification entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published members that unsubscirbed to the groups notification entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit members that unsubscirbed to the groups notification entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete members that unsubscirbed to the groups notification entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add members that unsubscirbed to the groups notification entities');
  }

}
